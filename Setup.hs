{-# LANGUAGE RecordWildCards #-}

import           Control.Exception                            (IOException,
                                                               catch)
import           Data.Semigroup                               ((<>))
import           Distribution.Simple
import qualified Distribution.Simple.InstallDirs              as InsDir
import           Distribution.Simple.Program.Builtin          (arProgram,
                                                               ghcProgram)
import qualified Distribution.Simple.Program.Db               as Db
import           Distribution.Simple.Program.Types            (ConfiguredProgram (..),
                                                               Program (..),
                                                               ProgramLocation (..))
import           Distribution.Simple.Setup                    (BuildFlags (..),
                                                               ConfigFlags (..),
                                                               Flag (..),
                                                               fromFlag)
import           Distribution.Simple.Utils                    (notice,
                                                               rawSystemExit,
                                                               rawSystemExitCode)
import qualified Distribution.Types.BuildInfo                 as BI
import           Distribution.Types.GenericPackageDescription (GenericPackageDescription)
import           Distribution.Types.HookedBuildInfo           (HookedBuildInfo)
import           Distribution.Types.LocalBuildInfo
import           Distribution.Types.PackageDescription        (PackageDescription)
import qualified Distribution.Types.PackageDescription        as PD
import           System.Directory                             (getCurrentDirectory,
                                                               makeAbsolute,
                                                               withCurrentDirectory)
import           System.Exit                                  (ExitCode (ExitSuccess))
import           System.FilePath                              ((</>))

main =
    let hooks = simpleUserHooks { confHook = myConfHook, buildHook = myBuild }
    in  do
            defaultMainWithHooks hooks


myBuild
    :: PackageDescription -> LocalBuildInfo -> UserHooks -> BuildFlags -> IO ()
myBuild pd lbi hooks flags = do
    let verbosity     = fromFlag (buildVerbosity flags)
    root <- makeAbsolute =<< getCurrentDirectory
    cabalBuildDir <- makeAbsolute (buildDir lbi)
    notice verbosity "Checking if the go compiler is present..."
    hasGo <-
        (  rawSystemExitCode verbosity "sh" ["-c", "go version >/dev/null 2>&1"]
            >>= (return . (== ExitSuccess))
            )
            `catch` (const (return False) :: IOException -> IO Bool)
    goBin <- if hasGo
        then notice verbosity "The go compiler is present" >> return "go"
        else do
            notice
                verbosity
                "The go compiler is not present. Compiling the go compiler from source."
            let go14Root = cabalBuildDir </> "go1.4"
                go19Root = cabalBuildDir </> "go1.9"
            rawSystemExit
                verbosity
                "make"
                ["go1.9", "GO14ROOT=" <> go14Root, "GO19ROOT=" <> go19Root]
            return (go19Root </> "bin" </> "go")
    rawSystemExit
        verbosity
        "make"
        [ "generated/lib/libauth.a"
--        , "generated/lib/libauth-dyn.so"
        , "GO=" <> goBin
        ]
    rawSystemExit verbosity "mkdir" ["-p", cabalBuildDir </> "go"]
    withCurrentDirectory (cabalBuildDir </> "go") $ rawSystemExit
        verbosity
        "ar"
        ["-x", root </> "generated/lib/libauth.a"]
    buildHook simpleUserHooks pd lbi hooks flags
  where
    resolveProgramLocation prog = case programLocation prog of
        UserSpecified path -> path
        FoundOnSystem path -> path

myConfHook
    :: (GenericPackageDescription, HookedBuildInfo)
    -> ConfigFlags
    -> IO LocalBuildInfo
myConfHook t f = do
    lbi  <- confHook simpleUserHooks t f
    root <- makeAbsolute =<< getCurrentDirectory
    let
        verbosity     = fromFlag (configVerbosity f)
        cabalBuildDir = buildDir lbi
        Just ar       = Db.lookupProgram arProgram (withPrograms lbi)
        arLocation    = resolveProgramLocation ar
        arWrapperPath = root </> "build-tools" </> "ar-wrapper.sh"
        arWrapper     = ar
            { programDefaultArgs = arLocation : programDefaultArgs ar
            , programLocation    = UserSpecified arWrapperPath
            , programOverrideEnv = ("CABAL_BUILD_DIR", Just cabalBuildDir)
                : programOverrideEnv ar
            }

        Just ghc       = Db.lookupProgram ghcProgram (withPrograms lbi)
        ghcLocation    = resolveProgramLocation ghc
        ghcWrapperPath = root </> "build-tools" </> "ghc-wrapper.sh"
        ghcWrapper     = ghc
            { programDefaultArgs = ghcLocation : programDefaultArgs ghc
            , programLocation    = UserSpecified ghcWrapperPath
            , programOverrideEnv = ("CABAL_BUILD_DIR", Just cabalBuildDir)
                : programOverrideEnv ghc
            }
        withPrograms' =
            Db.updateProgram arWrapper
                $ Db.updateProgram ghcWrapper
                $ withPrograms lbi

    return lbi { withPrograms = withPrograms' }
  where
    resolveProgramLocation prog = case programLocation prog of
        UserSpecified path -> path
        FoundOnSystem path -> path

