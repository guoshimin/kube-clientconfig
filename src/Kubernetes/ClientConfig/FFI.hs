{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE OverloadedStrings        #-}
{-# LANGUAGE RecordWildCards          #-}

module Kubernetes.ClientConfig.FFI (
  getClientSettings,
  TLSConfig(..),
  ClientSettings(..)
  ) where

import           Control.Monad             (forM)
import           Data.ByteString           (ByteString)
import qualified Data.ByteString.Char8     as B
import qualified Data.CaseInsensitive      as CI
import qualified Data.Map                  as Map
import           Data.Monoid               ((<>))
import           Data.Text                 (Text)
import qualified Data.Text                 as T
import qualified Foreign.C.String          as Foreign
import qualified Foreign.ForeignPtr        as Foreign
import qualified Foreign.Marshal.Alloc     as Foreign
import qualified Foreign.Ptr               as Foreign
import qualified Foreign.Storable          as Foreign
import           Kubernetes.ClientConfig.ClientSettings
import           Network.HTTP.Types        (Header)


foreign import ccall "GetClientSettings"
  c_get_client_settings :: IO (Foreign.Ptr (Either Foreign.CString CClientSettings))

foreign import ccall "&free_client_settings"
  p_free_client_settings :: Foreign.FunPtr (Foreign.Ptr (Either Foreign.CString CClientSettings) -> IO ())

safePeekCString :: Foreign.CString -> IO (Maybe String)
safePeekCString cstr = if cstr /= Foreign.nullPtr
    then Just <$> Foreign.peekCString cstr
    else return Nothing

safePeekCStringLen :: Foreign.CStringLen -> IO (Maybe ByteString)
safePeekCStringLen cstr = if fst cstr /= Foreign.nullPtr
    then (Just . B.pack) <$> Foreign.peekCStringLen cstr
    else return Nothing

data TLSConfig = TLSConfig
  { caFile     :: Maybe FilePath
  , certFile   :: Maybe FilePath
  , keyFile    :: Maybe FilePath
  , insecure   :: Bool
  , serverName :: Maybe Text
  , caData     :: Maybe ByteString
  , certData   :: Maybe ByteString
  , keyData    :: Maybe ByteString
  } deriving (Show, Eq)

fromCTLSConfig :: CTLSConfig -> IO TLSConfig
fromCTLSConfig CTLSConfig {..} = do
    caFile     <- safePeekCString caFile
    certFile   <- safePeekCString certFile
    keyFile    <- safePeekCString keyFile
    serverName <- (fmap T.pack) <$> safePeekCString serverName
    caData     <- safePeekCStringLen caData
    certData   <- safePeekCStringLen certData
    keyData    <- safePeekCStringLen keyData
    return TLSConfig {..}

fromCHeader :: CHeader -> IO Header
fromCHeader CHeader {..} = do
    name  <- (CI.mk . B.pack) <$> Foreign.peekCString name
    value <- B.pack <$> Foreign.peekCString value
    return (name, value)

fromCHeaderArray :: CHeaderArray -> IO [Header]
fromCHeaderArray CHeaderArray {..} =
    forM [0 .. length - 1] (\i -> Foreign.peekElemOff headers i >>= fromCHeader)

data ClientSettings = ClientSettings { headers   :: [Header]
                                     , tlsConfig :: TLSConfig } deriving (Show, Eq)

fromCGetClientSettingsReturn
    :: Either Foreign.CString CClientSettings
    -> IO (Either String ClientSettings)
fromCGetClientSettingsReturn (Right CClientSettings {..}) = do
    headers   <- fromCHeaderArray headers
    tlsConfig <- fromCTLSConfig tlsConfig
    return $ Right ClientSettings {..}
fromCGetClientSettingsReturn (Left error) = do
    err <- Foreign.peekCString error
    return $ Left err

getClientSettings :: IO (Either String ClientSettings)
getClientSettings = do
    ptr <-
        Foreign.newForeignPtr p_free_client_settings =<< c_get_client_settings
    ccsr <- Foreign.withForeignPtr ptr Foreign.peek
    fromCGetClientSettingsReturn ccsr
