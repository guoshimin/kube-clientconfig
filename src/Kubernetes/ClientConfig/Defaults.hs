{-# LANGUAGE OverloadedStrings         #-}

module Kubernetes.ClientConfig.Defaults where

import           System.FilePath            (FilePath, joinPath,
                                             splitSearchPath)
import           System.Directory           (getHomeDirectory)
import           System.Environment         (lookupEnv)

recommendedConfigPathEnvVar :: String
recommendedConfigPathEnvVar = "KUBECONFIG"

recommendedHomeDir :: FilePath
recommendedHomeDir = ".kube"

recommendedFileName :: FilePath
recommendedFileName = "config"

recommendedConfigDir :: IO FilePath
recommendedConfigDir = do
    home <- getHomeDirectory
    return $ joinPath [home, recommendedHomeDir]

recommendedHomeFile :: IO FilePath
recommendedHomeFile = do
    configDir <- recommendedConfigDir
    return $ joinPath [configDir, recommendedFileName]

defaultChain :: IO [FilePath]
defaultChain = do
    maybeEnvVarFiles <- lookupEnv recommendedConfigPathEnvVar
    case maybeEnvVarFiles of
        Just ""          -> return <$> recommendedHomeFile
        Nothing          -> return <$> recommendedHomeFile
        Just envVarFiles -> return (splitSearchPath envVarFiles)

