{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE FunctionalDependencies    #-}
{-# LANGUAGE KindSignatures            #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RecordWildCards           #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE UndecidableInstances      #-}

{-|
Module      : Kubernetes.ClientConfig.Config
Description : Data model for the kubeconfig.

This module contains the definition of the data model of the kubeconfig.

The official definition of the kubeconfig is defined in https://github.com/kubernetes/client-go/blob/master/tools/clientcmd/api/v1/types.go.

This is a mostly straightforward translation into Haskell, with 'FromJSON' and 'ToJSON' instances defined.
-}
module Kubernetes.ClientConfig.Config where

import           Control.Applicative         (Alternative ((<|>)))
import           Control.Error.Util          (note)
import           Control.Monad.Except        (ExceptT, MonadError, throwError)
import           Control.Monad.IO.Class      (MonadIO, liftIO)
import           Control.Monad.Trans.Class   (lift)
import           Control.Monad.Trans.Maybe   (MaybeT (..))
import           Data.Aeson                  (FromJSON (..), Options,
                                              ToJSON (..), Value (..), camelTo2,
                                              defaultOptions,
                                              fieldLabelModifier,
                                              genericParseJSON, genericToJSON,
                                              object, omitNothingFields,
                                              withArray, withObject, (.:), (.=))
import           Data.ByteString             (ByteString)
import qualified Data.ByteString             as B
import qualified Data.ByteString.Base64      as Base64
import qualified Data.ByteString.Char8       as Char8
import qualified Data.ByteString.Lazy        as LazyB
import qualified Data.CaseInsensitive        as CI
import           Data.Default.Class          (def)
import           Data.Either                 (rights)
import qualified Data.IP                     as IP
import           Data.Map                    (Map)
import qualified Data.Map                    as Map
import           Data.PEM                    (pemContent, pemParseBS)
import           Data.Proxy
import           Data.Semigroup              ((<>))
import           Data.Text                   (Text)
import qualified Data.Text                   as T
import qualified Data.Text.Encoding          as T
import qualified Data.Text.IO                as TIO
import           Data.Traversable            (for)
import qualified Data.Vector                 as V
import           Data.X509                   (SignedCertificate,
                                              decodeSignedCertificate)
import qualified Data.X509                   as X509
import           Data.X509.CertificateStore  (makeCertificateStore)
import qualified Data.X509.Validation        as X509
import           Data.Yaml                   (decodeEither)
import           GHC.Generics
import           GHC.TypeLits
import qualified Kubernetes.ClientConfig.FFI as FFI
import           Kubernetes.OpenAPI          (AuthApiKeyBearerToken (..))
import           Kubernetes.OpenAPI.Core     (AnyAuthMethod (..),
                                              KubernetesClientConfig,
                                              configAuthMethods, configHost)
import qualified Kubernetes.OpenAPI.Core     as K
import qualified Lens.Micro                  as L
import           Network.Connection          (TLSSettings (..))
import qualified Network.HTTP.Client         as NH
import           Network.HTTP.Client.TLS     (mkManagerSettings)
import           Network.HTTP.Types          (Header, RequestHeaders)
import           Network.TLS                 (ClientParams (..), Credential,
                                              credentialLoadX509FromMemory,
                                              defaultParamsClient,
                                              onCertificateRequest,
                                              onServerCertificate,
                                              sharedCAStore)
import qualified Network.TLS                 as TLS
import qualified Network.TLS.Extra           as TLS
import qualified Network.URI                 as URI
import           System.FilePath             (FilePath)
import           System.Process              (readProcess)
import           System.X509                 (getSystemCertificateStore)
import           Text.Read                   (readMaybe)

camelToWithOverrides :: Char -> Map String String -> Options
camelToWithOverrides c overrides = defaultOptions
    { fieldLabelModifier = modifier
    , omitNothingFields  = True
    }
    where modifier s = Map.findWithDefault (camelTo2 c s) s overrides

-- |Represents a kubeconfig.
data Config = Config
  { kind           :: Maybe (Text)
  , apiVersion     :: Maybe (Text)
  , preferences    :: Preferences
  , clusters       :: NamedEntities Cluster "cluster"
  , authInfos      :: NamedEntities AuthInfo "user"
  , contexts       :: NamedEntities Context "context"
  , currentContext :: Text
  } deriving (Generic, Show)

configJSONOptions = camelToWithOverrides
    '-'
    (Map.fromList [("apiVersion", "apiVersion"), ("authInfos", "users")])

instance ToJSON Config where
  toJSON = genericToJSON configJSONOptions

instance FromJSON Config where
  parseJSON = genericParseJSON configJSONOptions

newtype Preferences = Preferences
  { colors :: Maybe (Bool)
  } deriving (Generic, Show)

instance ToJSON Preferences where
  toJSON = genericToJSON $ camelToWithOverrides '-' Map.empty

instance FromJSON Preferences where
  parseJSON = genericParseJSON $ camelToWithOverrides '-' Map.empty

data Cluster = Cluster
  { server                   :: Text
  , insecureSkipTLSVerify    :: Maybe (Bool)
  , certificateAuthority     :: Maybe (Text)
  , certificateAuthorityData :: Maybe (Text)
  } deriving (Generic, Show)

instance ToJSON Cluster where
  toJSON = genericToJSON $ camelToWithOverrides '-' Map.empty

instance FromJSON Cluster where
  parseJSON = genericParseJSON $ camelToWithOverrides '-' Map.empty

{-|
  Parsing a JSON structure like

@
[
  {
    "name": "foo",
    "/ENTITY/": {...}
  },
  {
    "name": "bar",
    "/ENTITY/": {...}
  },
  ...
]
@

  into a Map from name to /ENTITY/, where the actual value of the string "/ENTITY/" is reified in the type variable @s@.
-}
newtype NamedEntities a (s :: Symbol) = NamedEntities
  { unNamedEntities :: Map Text a
  } deriving (Show)

instance (FromJSON a, KnownSymbol s) =>
         FromJSON (NamedEntities a s) where
  parseJSON =
    withArray "stuffs" $ \arr -> NamedEntities <$> makeMap (for arr makeTuple)
    where
      makeTuple =
        withObject "NamedStuff" $ \v ->
          (,) <$> v .: "name" <*> v .: T.pack (symbolVal (Proxy :: Proxy s))
      makeMap = fmap (Map.fromList . V.toList)


array :: [Value] -> Value
array = Array . V.fromList

instance (ToJSON a, KnownSymbol s) =>
         ToJSON (NamedEntities a s) where
  toJSON (NamedEntities mapping) = array $ map makeObj (Map.assocs mapping)
    where
      makeObj (k, v) =
        object
          ["name" .= toJSON k, T.pack (symbolVal (Proxy :: Proxy s)) .= toJSON v]

data AuthInfo = AuthInfo
  { clientCertificate     :: Maybe (FilePath)
  , clientCertificateData :: Maybe (Text)
  , clientKey             :: Maybe (FilePath)
  , clientKeyData         :: Maybe (Text)
  , token                 :: Maybe (Text)
  , tokenFile             :: Maybe (FilePath)
  , impersonate           :: Maybe (Text)
  , impersonateGroups     :: Maybe ([Text])
  , impersonateUserExtra  :: Maybe (Map Text [Text])
  , username              :: Maybe (Text)
  , password              :: Maybe (Text)
  , authProvider          :: Maybe (AuthProviderConfig)
  } deriving (Generic, Show)

authInfoJSONOptions = camelToWithOverrides
    '-'
    ( Map.fromList
        [ ("tokenFile"           , "tokenFile")
        , ("impersonate"         , "as")
        , ("impersonateGroups"   , "as-groups")
        , ("impersonateUserExtra", "as-user-extra")
        ]
    )

instance ToJSON AuthInfo where
  toJSON = genericToJSON authInfoJSONOptions

instance FromJSON AuthInfo where
  parseJSON = genericParseJSON authInfoJSONOptions

data Context = Context
  { cluster   :: Text
  , authInfo  :: Text
  , namespace :: Maybe Text
  } deriving (Generic, Show)

contextJSONOptions =
    camelToWithOverrides '-' (Map.fromList [("authInfo", "user")])

instance ToJSON Context where
  toJSON = genericToJSON contextJSONOptions

instance FromJSON Context where
  parseJSON = genericParseJSON contextJSONOptions

data AuthProviderConfig = AuthProviderConfig
  { name   :: Text
  , config :: Maybe (Map Text Text)
  } deriving (Generic, Show)

instance ToJSON AuthProviderConfig where
  toJSON = genericToJSON $ camelToWithOverrides '-' Map.empty

instance FromJSON AuthProviderConfig where
  parseJSON = genericParseJSON $ camelToWithOverrides '-' Map.empty


-- |Returns the currently active context.
getContext :: Config -> Either String Context
getContext Config {..} =
    let maybeContext = Map.lookup currentContext (unNamedEntities contexts)
    in  case maybeContext of
            Just ctx -> Right ctx
            Nothing  -> Left ("No context named " <> T.unpack currentContext)

-- |Returns the currently active user.
getAuthInfo :: Config -> Either String (Text, AuthInfo)
getAuthInfo cfg@Config {..} = do
    Context {..} <- getContext cfg
    let maybeAuth = Map.lookup authInfo (unNamedEntities authInfos)
    case maybeAuth of
        Just auth -> Right (authInfo, auth)
        Nothing   -> Left ("No user named " <> T.unpack authInfo)

-- |Returns the currently active cluster.
getCluster :: Config -> Either String Cluster
getCluster cfg@Config {..} = do
    Context {..} <- getContext cfg
    let maybeCluster = Map.lookup cluster (unNamedEntities clusters)
    case maybeCluster of
        Just cluster -> Right cluster
        Nothing      -> Left ("No cluster named " <> T.unpack cluster)
