{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}

module Kubernetes.ClientConfig.ClientSettings where

import qualified Foreign.C.String as Foreign
import qualified Foreign.Ptr as Foreign
import Foreign.C.Types
import Control.Applicative ((<$>), (<*>))
import Foreign.Storable

#include <auth.h>

data CTLSConfig = CTLSConfig
  { caFile :: Foreign.CString
  , certFile :: Foreign.CString
  , keyFile :: Foreign.CString
  , insecure :: Bool
  , serverName :: Foreign.CString
  , caData :: Foreign.CStringLen
  , certData :: Foreign.CStringLen
  , keyData :: Foreign.CStringLen
  } deriving (Show, Eq)

instance Storable CTLSConfig where
  sizeOf _ = #{size struct TLSConfig}
  alignment _ = #{alignment struct TLSConfig}
  peek ptr = CTLSConfig
    <$> (#peek struct TLSConfig, CAFile) ptr
    <*> (#peek struct TLSConfig, CertFile) ptr
    <*> (#peek struct TLSConfig, KeyFile) ptr
    <*> (#peek struct TLSConfig, Insecure) ptr
    <*> (#peek struct TLSConfig, ServerName) ptr
    <*> ((,) <$> (#peek struct TLSConfig, CAData) ptr <*> (#peek struct TLSConfig, CADataLen) ptr)
    <*> ((,) <$> (#peek struct TLSConfig, CertData) ptr <*> (#peek struct TLSConfig, CertDataLen) ptr)
    <*> ((,) <$> (#peek struct TLSConfig, KeyData) ptr <*> (#peek struct TLSConfig, KeyDataLen) ptr)
  poke p CTLSConfig{..} = do
    #{poke struct TLSConfig, CAFile} p caFile
    #{poke struct TLSConfig, CertFile} p certFile
    #{poke struct TLSConfig, KeyFile} p keyFile
    #{poke struct TLSConfig, Insecure} p insecure
    #{poke struct TLSConfig, ServerName} p serverName
    #{poke struct TLSConfig, CAData} p $ fst caData
    #{poke struct TLSConfig, CertData} p $ fst certData
    #{poke struct TLSConfig, KeyData} p $ fst keyData
    #{poke struct TLSConfig, CADataLen} p $ snd caData
    #{poke struct TLSConfig, CertDataLen} p $ snd certData
    #{poke struct TLSConfig, KeyDataLen} p $ snd keyData

data CHeader = CHeader { name :: Foreign.CString
                       , value :: Foreign.CString } deriving (Show, Eq)

instance Storable CHeader where
  sizeOf _ = #{size struct Header}
  alignment _ = #{alignment struct Header}
  peek ptr = CHeader
    <$> (#peek struct Header, name) ptr
    <*> (#peek struct Header, value) ptr
  poke p CHeader{..} = do
    #{poke struct Header, name} p name
    #{poke struct Header, value} p value

data CHeaderArray = CHeaderArray { headers :: Foreign.Ptr CHeader
                                 , length :: Int } deriving (Show, Eq)

instance Storable CHeaderArray where
  sizeOf _ = #{size struct HeaderArray}
  alignment _ = #{alignment struct HeaderArray}
  peek ptr = CHeaderArray
    <$> (#peek struct HeaderArray, headers) ptr
    <*> (#peek struct HeaderArray, length) ptr
  poke p CHeaderArray {..}  = do
    #{poke struct HeaderArray, headers} p headers
    #{poke struct HeaderArray, length} p length

data CClientSettings = CClientSettings { headers :: CHeaderArray
                                       , tlsConfig :: CTLSConfig } deriving (Show, Eq)

instance Storable (Either Foreign.CString CClientSettings) where
  sizeOf _ = #{size struct ClientSettings}
  alignment _ = #{alignment struct ClientSettings}
  peek ptr = do
    error <- (#peek struct ClientSettings, error) ptr
    if error == Foreign.nullPtr
      then CClientSettings
           <$> (#peek struct ClientSettings, headers) ptr
           <*> (#peek struct ClientSettings, tls_config) ptr >>= (return . Right)
      else return (Left error)

  poke p (Right CClientSettings {..}) = do
    #{poke struct ClientSettings, headers} p headers
    #{poke struct ClientSettings, tls_config} p tlsConfig

  poke p (Left error) = do
    #{poke struct ClientSettings, error} p error
