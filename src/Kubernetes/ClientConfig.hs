{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RecordWildCards       #-}

module Kubernetes.ClientConfig where

import           Control.Applicative            (Alternative ((<|>)))
import           Control.Monad.Except           (ExceptT, MonadError,
                                                 throwError)
import           Control.Monad.IO.Class         (MonadIO, liftIO)
import           Control.Monad.Trans.Class      (lift)
import           Data.ByteString                (ByteString)
import qualified Data.ByteString                as B
import qualified Data.ByteString.Lazy           as LazyB
import qualified Data.CaseInsensitive           as CI
import           Data.Default.Class             (def)
import           Data.Either                    (rights)
import qualified Data.IP                        as IP
import qualified Data.Map                       as Map
import           Data.PEM                       (pemContent, pemParseBS)
import           Data.Semigroup                 ((<>))
import           Data.Text                      (Text)
import qualified Data.Text                      as T
import qualified Data.Text.Encoding             as T
import           Data.Traversable               (for)
import           Data.X509                      (SignedCertificate,
                                                 decodeSignedCertificate)
import qualified Data.X509                      as X509
import           Data.X509.CertificateStore     (makeCertificateStore)
import qualified Data.X509.Validation           as X509
import           Data.Yaml                      (decodeEither)
import           Kubernetes.ClientConfig.Config
import qualified Kubernetes.ClientConfig.FFI    as FFI
import           Kubernetes.OpenAPI             (AuthApiKeyBearerToken (..))
import           Kubernetes.OpenAPI.Core        (AnyAuthMethod (..),
                                                 KubernetesClientConfig,
                                                 configAuthMethods, configHost)
import qualified Kubernetes.OpenAPI.Core        as K
import qualified Lens.Micro                     as L
import           Network.Connection             (TLSSettings (..))
import qualified Network.HTTP.Client            as NH
import           Network.HTTP.Client.TLS        (mkManagerSettings)
import           Network.HTTP.Types             (Header, RequestHeaders)
import           Network.TLS                    (ClientParams (..), Credential,
                                                 credentialLoadX509FromMemory,
                                                 defaultParamsClient,
                                                 onCertificateRequest,
                                                 onServerCertificate,
                                                 sharedCAStore)
import qualified Network.TLS                    as TLS
import qualified Network.TLS.Extra              as TLS
import qualified Network.URI                    as URI
import           System.FilePath                (FilePath)
import           System.X509                    (getSystemCertificateStore)
import           Text.Read                      (readMaybe)


class Monad m =>
      MonadFile m where
  myReadFile :: FilePath -> m ByteString
  myWriteFile :: FilePath -> ByteString -> m ()

instance MonadFile IO where
  myReadFile = B.readFile
  myWriteFile = B.writeFile

instance MonadFile m => MonadFile (ExceptT e m) where
  myReadFile = lift . myReadFile
  myWriteFile = (lift .) . myWriteFile

-- |Which CA to use to validate the server's certificate.
data ServerCA
  = Insecure                     -- ^ Don't validate the certificate
  | CustomCA [SignedCertificate] -- ^ Using the specified CA certs
  | SystemCA                     -- ^ Using the system CA store
  deriving (Show, Eq)

-- |How to validate the server's certificate and what client certificate to use.
data TLSMutualAuth = TLSMutualAuth
  { serverCA   :: ServerCA
  , credential :: Maybe Credential
  } deriving (Show, Eq)

-- | Loads a PEM-encoded @ByteString@ as a list of certificates.
loadPEMCert :: MonadError String m => ByteString -> m [SignedCertificate]
loadPEMCert b = do
    pems <- either throwError return (pemParseBS b)
    return $ rights $ map (decodeSignedCertificate . pemContent) pems

-- | Transforms 'FFI.TLSConfig', which mirrors the structure of the same name in Go, to 'TLSMutualAuth',
-- which is easier to work with.
getTLSMutualAuth
    :: (MonadFile m, MonadError String m) => FFI.TLSConfig -> m TLSMutualAuth
getTLSMutualAuth FFI.TLSConfig {..} = do
    serverCA <- case insecure of
        True -> case (caFile, caData) of
            (Nothing, Nothing) -> return Insecure
            _ ->
                throwError
                    "specifying a root certificates file with the insecure flag is not allowed"
        _ -> case (caFile, caData) of
            (Nothing, Nothing) -> return SystemCA
            (_      , Just d ) -> CustomCA <$> loadPEMCert d
            (Just fn, _      ) -> CustomCA <$> (myReadFile fn >>= loadPEMCert)
    certData   <- readFromMemoryOrFile certData certFile
    keyData    <- readFromMemoryOrFile keyData keyFile
    credential <- case (certData, keyData) of
        (Just cert, Just key) ->
            either throwError (return . Just)
                $ credentialLoadX509FromMemory cert key
        (Just cert, Nothing) ->
            throwError
                "client-key-data or client-key must be specified to use the clientCert authentication method."
        (Nothing, _) -> return Nothing
    return TLSMutualAuth {..}
  where
    readFromMemoryOrFile maybeData maybePath = case (maybeData, maybePath) of
        (Just d , _        ) -> return (Just d)
        (_      , Just path) -> fmap Just (myReadFile path)
        (Nothing, Nothing  ) -> return Nothing

-- | Given a 'TLSMutualAuth', produce a function that can set the fields in a 'ClientParams'
-- according to the 'TLSMutualAuth'.
setTLSClientParams
    :: MonadIO m => TLSMutualAuth -> m (ClientParams -> ClientParams)
setTLSClientParams TLSMutualAuth {..} = do
    serverCAConf <- case serverCA of
        Insecure -> return $ \p -> p
            { clientHooks = (clientHooks p)
                { onServerCertificate = \_ _ _ _ -> return []
                }
            }
        SystemCA ->
            liftIO getSystemCertificateStore
                >>= ( \s ->
                        return $ \p ->
                            p { clientShared = def { sharedCAStore = s } }
                    )
        CustomCA certs ->
            return
                $ \p -> p
                      { clientShared = def
                          { sharedCAStore = makeCertificateStore certs
                          }
                      }
    clientCertConf <- case credential of
        Just cred -> return $ \p -> p
            { clientHooks = (clientHooks p)
                { onCertificateRequest = \_ -> return (Just cred)
                }
            }
        Nothing -> return id
    return (serverCAConf . clientCertConf)

-- | Produces a function that can configure the given 'KubernetesConfig' according to the server address and authentication headers.
setKubernetesConfig
    :: MonadError String m
    => Text           -- ^ server address
    -> RequestHeaders -- ^ Authentication headers to add
    -> m (KubernetesClientConfig -> KubernetesClientConfig)
setKubernetesConfig server headersToAdd = do
    let authMethods = getAuthMethods headersToAdd
    return $ \kcfg -> kcfg
        { configHost        = (LazyB.fromStrict . T.encodeUtf8) server
        , configAuthMethods = authMethods
        }

-- Default TLS settings with an option to disable to server name validation.
-- The TLS library doesn't use the IP fields in the subjectAltNames for server validation,
-- so if the server is specified using an IP address, we need to turn off server name validation.
defaultTLSClientParams
    :: Bool -- ^ Whether to validate server name
    -> ClientParams
defaultTLSClientParams validateServerName =
    let defParams = defaultParamsClient "" ""
    in  defParams
            { TLS.clientSupported = def
                { TLS.supportedCiphers = TLS.ciphersuite_strong
                }
            , TLS.clientHooks     = (TLS.clientHooks defParams)
                { TLS.onServerCertificate = X509.validate
                    X509.HashSHA256
                    def
                    def { X509.checkFQHN = validateServerName }
                }
            }

addressIsIP :: Text -> Maybe Bool
addressIsIP addr = do
    uri  <- URI.parseAbsoluteURI (T.unpack addr)
    host <- URI.uriRegName <$> (URI.uriAuthority uri)
    ((readMaybe host :: Maybe IP.IP) >> return True) <|> return False

-- |A type that encapsulates the first two parameters of the @dispatch*@ functions in 'Kubernetes.Client'.
-- These are the parameters dictated by the kubeconfig.
newtype KubernetesClient = KubernetesClient (NH.Manager, KubernetesClientConfig)

-- |Creates a 'KubernetesClient' from the given kubeconfig file.
getClient
    :: (MonadIO m, MonadFile m, MonadError String m)
    => FilePath  -- ^ Path to the kubeconfig file.
    -> m KubernetesClient
getClient filename = do
    configContent <- myReadFile filename
    cfg           <- either throwError
                            return
                            (decodeEither configContent :: Either String Config)
    Cluster { server = server } <- either throwError return (getCluster cfg)
    isIP                        <- maybe
        (throwError ("Failed to parse" <> T.unpack server <> " as a URI"))
        return
        (addressIsIP server)
    FFI.ClientSettings {..} <-
        liftIO FFI.getClientSettings >>= either throwError return
    -- setting configValidateAuthMethods to False below because a client can use client cert auth,
    -- but the OpenAPI spec has everything using Authorization header.
    kConf <- setKubernetesConfig server headers <*> liftIO
        (fmap (\x -> x { K.configValidateAuthMethods = False }) K.newConfig)
    tlsConf      <- getTLSMutualAuth tlsConfig
    clientParams <- setTLSClientParams tlsConf
        <*> return (defaultTLSClientParams (not isIP))
    let tlsSettings = TLSSettings clientParams
        ms          = mkManagerSettings tlsSettings Nothing
    manager <- liftIO (NH.newManager ms)
    return (KubernetesClient (manager, kConf))

-- |A convenience function for calling the @dispatch*@ functions in 'Kubernetes.Client',
-- e.g.,
--
-- @
-- resp <- withKubernetesClient client dispatchMime req
-- @
withKubernetesClient :: KubernetesClient -> (NH.Manager -> KubernetesClientConfig -> a) -> a
withKubernetesClient (KubernetesClient (manager, kConf)) f = f manager kConf

addHeader
    :: K.KubernetesRequest req contentType res accept
    -> Header
    -> K.KubernetesRequest req contentType res accept
addHeader req header = L.over (K.rParamsL . K.paramsHeadersL) (header :) req

newtype HTTPHeaderAuth = HTTPHeaderAuth Header

instance K.AuthMethod HTTPHeaderAuth where
  applyAuthMethod _ (HTTPHeaderAuth header) req = return $ req `addHeader` header

getAuthMethods :: RequestHeaders -> [AnyAuthMethod]
getAuthMethods headers = map
    ( \h@(name, value) -> if name == CI.mk "Authorization"
        then AnyAuthMethod (AuthApiKeyBearerToken (T.decodeUtf8 value))
        else AnyAuthMethod (HTTPHeaderAuth h)
    )
    headers
