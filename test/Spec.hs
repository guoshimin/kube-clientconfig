{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE TypeSynonymInstances       #-}

module Main where

--import Test.Hspec.Expectations
import           Control.Monad                  (forever)
import           Control.Monad.Except           (runExceptT)
import           Control.Monad.State            (StateT, get, put, runStateT)
import           Control.Monad.Trans.Class      (MonadTrans)
import           Data.Aeson                     (eitherDecode)
import           Data.ByteString                (ByteString)
import qualified Data.ByteString.Char8          as Char8
import           Data.Function                  ((&))
import           Data.Functor.Identity          (runIdentity)
import           Data.Map                       (Map)
import qualified Data.Map                       as Map
import           Data.Semigroup                 ((<>))
import           Data.Text                      (Text)
import           Kubernetes.ClientConfig
import           Kubernetes.ClientConfig.Config
import           Kubernetes.ClientConfig.FFI
import qualified Kubernetes.OpenAPI             as K
import qualified Kubernetes.OpenAPI.API.Core    as Core
import qualified Kubernetes.OpenAPI.API.CoreV1  as CoreV1
import qualified Kubernetes.OpenAPI.Client      as K
import qualified Kubernetes.OpenAPI.Core        as K
import qualified Kubernetes.OpenAPI.MimeTypes   as K
import qualified Kubernetes.OpenAPI.Model       as K
import           Test.Hspec
import           Text.RawString.QQ

newtype MockFileT m a =
  MockFileT (StateT (Map FilePath ByteString) m a)
  deriving (Functor, Applicative, Monad, MonadTrans)

runMockFileT :: Map FilePath ByteString
             -> MockFileT m a
             -> m (a, Map FilePath ByteString)
runMockFileT fs (MockFileT x) = runStateT x fs

runMockFileTGetFile
  :: Monad m
  => Map FilePath ByteString -> FilePath -> MockFileT m a -> m ByteString
runMockFileTGetFile fs filename mf =
  fst <$> runMockFileT fs (mf >> myReadFile filename)

instance (Monad m) =>
         MonadFile (MockFileT m) where
  myReadFile path =
    MockFileT $ do
      files <- get
      maybe (fail (path <> "not found")) return (Map.lookup path files)
  myWriteFile path b =
    MockFileT $ do
      cache <- get
      put (Map.insert path b cache)


main :: IO ()
main = do
  hspec $ do
    describe "Config.addressIsIP" $ do
      it "returns (Just True) for URIs whose host portion is an IP address" $ do
        addressIsIP "http://1.1.1.1" `shouldBe` Just True
      it "returns (Just False) for URIs whose host portion is not an IP address" $ do
        addressIsIP "http://example.com" `shouldBe` Just False
      it "returns Nothing for input that's not a valid URI" $ do
        addressIsIP "foobar" `shouldBe` Nothing
--------------------
  putStrLn "hello"
  client <- runExceptT (getClient "/home/sguo/.kube/config") >>= either fail return
  withKubernetesClient client K.dispatchMime (Core.getAPIVersions (K.Accept K.MimeJSON)) >>= print
  ns <- either error return (eitherDecode "{\"metadata\": {\"name\": \"foobar2\"}}" :: Either String K.V1Namespace)
  withKubernetesClient client K.dispatchMime (CoreV1.createNamespace (K.ContentType K.MimeJSON) (K.Accept K.MimeJSON) ns) >>= print
  getClientSettings >>= print
  return ()
  where
    kubeConfig =
      [r| apiVersion: v1
 clusters:
 - cluster:
     certificate-authority: /home/shimin/.minikube/ca.crt
     server: https://172.29.0.100:8443
   name: minikube
 contexts:
 - context:
     cluster: minikube
     user: minikube
   name: minikube
 current-context: minikube
 kind: Config
 preferences: {}
 users:
 - name: minikube
   user:
     auth-provider:
       config:
         cmd-args: config config-helper --format=json
         cmd-path: /home/shimin/google-cloud-sdk/bin/gcloud
         expiry: 2017-11-22T01:57:20Z
         expiry-key: '{.credential.token_expiry}'
         token-key: '{.credential.access_token}'
       name: gcp
|]
