#include <stdlib.h>
#include <clientsettings.h>
#include <string.h>

struct ClientSettings* AllocClientSettings() {
    struct ClientSettings *ret =  (struct ClientSettings*) malloc(sizeof(struct ClientSettings));
    memset(ret, 0, sizeof(struct ClientSettings));
    return ret;
}

struct Header* AllocHeaders(unsigned n) {
    struct Header *ret = (struct Header*) malloc(n * sizeof(struct Header));
    memset(ret, 0, sizeof(n * sizeof(struct Header)));
    return ret;
}

struct Header* HeaderAt(struct Header* base, int n) {
    return base + n;
}
