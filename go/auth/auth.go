// This package contains bindings that export functionalities in the k8s.io/client-go repo related to KUBECONFIG handling for use by C.
// The exported function, GetClientSettings, allows the calling C code to get
// - HTTP headers that should be set on requests,
// - TLS parameters, such as CA cert, client cert, etc.
package main

//#cgo CFLAGS: -std=c99 -I../../include
//#include <clientsettings.h>
import "C"

import (
	"net/http"
	"unsafe"

	_ "k8s.io/client-go/plugin/pkg/client/auth/azure"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	_ "k8s.io/client-go/plugin/pkg/client/auth/oidc"
	_ "k8s.io/client-go/plugin/pkg/client/auth/openstack"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/transport"
)

func main() {
}

// An http.RoundTripper that simply records the HTTP request that passes through it.
type capturingRT struct {
	req http.Request
}

func (rt *capturingRT) RoundTrip(req *http.Request) (*http.Response, error) {
	rt.req = *req
	return &http.Response{}, nil
}

var _ http.RoundTripper = &capturingRT{}

//export GetClientSettings
func GetClientSettings() *C.struct_ClientSettings {
	ret := C.AllocClientSettings()
	transConf, err := getTransportConfig()
	if err != nil {
		ret.error = C.CString(err.Error())
		return ret
	}
	headers, n, err := getHeaders(transConf)
	if err != nil {
		ret.error = C.CString(err.Error())
		return ret
	}
	ret.headers = C.struct_HeaderArray{
		headers: headers,
		length:  C.size_t(n),
	}
	ret.tls_config = convertTLSConfig(transConf.TLS)
	return ret
}

func getHeaders(transConf transport.Config) (*C.struct_Header, int, error) {
	rt := &capturingRT{}
	wrapper, err := transport.HTTPWrappersForConfig(&transConf, rt)
	if err != nil {
		return (*C.struct_Header)(nil), 0, err
	}
	req, _ := http.NewRequest("", "", nil)
	wrapper.RoundTrip(req)
	numHeaders := 0
	for _, v := range rt.req.Header {
		numHeaders += len(v)
	}
	if len(rt.req.Header) == 0 {
		return (*C.struct_Header)(nil), 0, nil
	}
	ret := C.AllocHeaders(C.uint(numHeaders))
	idx := 0
	for k, vs := range rt.req.Header {
		for _, v := range vs {
			h := C.HeaderAt(ret, C.int(idx))
			h.name = C.CString(k)
			h.value = C.CString(v)
			idx += 1
		}
	}
	return ret, numHeaders, nil
}

func getTransportConfig() (transport.Config, error) {
	var overrides clientcmd.ConfigOverrides // TODO: allow overrides to be passed in
	cfg, err := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(clientcmd.NewDefaultClientConfigLoadingRules(), &overrides).ClientConfig()
	if err != nil {
		return transport.Config{}, err
	}
	transConf, err := cfg.TransportConfig()
	if err != nil {
		return transport.Config{}, err
	}
	return *transConf, nil
}

func cStringNullIfEmpty(s string) *C.char {
	if len(s) == 0 {
		return (*C.char)(nil)
	}
	return C.CString(s)
}

func cBytesNullIfEmpty(s []byte) unsafe.Pointer {
	if len(s) == 0 {
		return unsafe.Pointer(nil)
	}
	return C.CBytes(s)
}

func convertTLSConfig(tlsConf transport.TLSConfig) C.struct_TLSConfig {
	return C.struct_TLSConfig{
		CAFile:      cStringNullIfEmpty(tlsConf.CAFile),
		CertFile:    cStringNullIfEmpty(tlsConf.CertFile),
		KeyFile:     cStringNullIfEmpty(tlsConf.KeyFile),
		Insecure:    C._Bool(tlsConf.Insecure),
		ServerName:  cStringNullIfEmpty(tlsConf.ServerName),
		CAData:      cBytesNullIfEmpty(tlsConf.CAData),
		CertData:    cBytesNullIfEmpty(tlsConf.CertData),
		KeyData:     cBytesNullIfEmpty(tlsConf.KeyData),
		CADataLen:   C.size_t(len(tlsConf.CAData)),
		CertDataLen: C.size_t(len(tlsConf.CertData)),
		KeyDataLen:  C.size_t(len(tlsConf.KeyData)),
	}
}
