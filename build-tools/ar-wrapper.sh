#!/usr/bin/env bash

set -ue

set +e
# The `tr` below turns Windows path separators into Unix ones for the `grep` to
# succeed.
echo "$@" | tr '\\' '/' | grep "Kubernetes/ClientConfig/FFI" > /dev/null 2>&1

RC=$?
set -e

if [[ ${RC} -eq 0 ]]; then
    exec $@ ${CABAL_BUILD_DIR}/go/*.o
else
    exec $@
fi
