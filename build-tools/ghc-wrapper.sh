#!/usr/bin/env bash

set -ue

set +e
# The `tr` below turns Windows path separators into Unix ones for the `grep` to
# succeed.
echo "$@" | tr '\\' '/' | grep "Kubernetes/ClientConfig/FFI\.[a-z_]*o" > /dev/null 2>&1
RC=$?
set -e

set +e
$1 --version | grep 'version 8' > /dev/null 2>&1
GHC8=$?
set -e

if [[ ${RC} -eq 0 ]]; then
    exec $@ ${CABAL_BUILD_DIR}/go/*.o
else
    exec $@
    # if test ${GHC8} -eq 0; then
    #     exec $@ -fno-warn-redundant-constraints
    # else
    #     exec $@
    # fi
fi
