#!/usr/bin/env bash

set -e

if [[ -z ${GOROOT_FINAL} ]]; then
    echo "\$GOROOT_FINAL must be set" >&2
    exit 1
fi

if [[ ${GOROOT_FINAL} != /*/* ]]; then
    echo "\$GOROOT_FINAL must be absolute and have at least two slashes. Got $GOROOT_FINAL" >&2
    exit 1
fi

if ${GOROOT_FINAL}/bin/go env > /dev/null 2>&1; then
    echo "Go already installed at ${GOROOT_FINAL}. Exiting."
    exit 0
fi

rm -rf go1.4 || true
mkdir go1.4
(
    cd go1.4
    tar -xf ../go1.4-bootstrap-20170531.tar.gz --strip-components=1
)

(
    cd go1.4/src
    GOROOT_FINAL=${GOROOT_FINAL} ./make.bash
)

rm -rf ${GOROOT_FINAL}
mv go1.4 ${GOROOT_FINAL}
