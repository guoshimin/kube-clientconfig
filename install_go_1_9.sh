#!/usr/bin/env bash

set -e

if [[ -z ${GOROOT_FINAL} || -z ${GOROOT_BOOTSTRAP} ]]; then
    echo "\$GOROOT_FINAL and \$GOROOT_BOOTSTRAP must be set" >&2
    exit 1
fi

if [[ ${GOROOT_FINAL} != /*/* ]]; then
    echo "\$GOROOT_FINAL must be absolute and have at least two slashes. Got $GOROOT_FINAL" >&2
    exit 1
fi

if ${GOROOT_FINAL}/bin/go env > /dev/null 2>&1; then
    echo "Go already installed at ${GOROOT_FINAL}. Exiting."
    exit 0
fi

rm -rf go1.9 || true
mkdir go1.9
(
    cd go1.9
    tar -xf ../go1.9.2.tar.gz
)

(
    cd go1.9/src
    GOROOT_FINAL=${GOROOT_FINAL} GOROOT_BOOTSTRAP=${GOROOT_BOOTSTRAP} ./make.bash
)

rm -rf ${GOROOT_FINAL}
mv go1.9 ${GOROOT_FINAL}
