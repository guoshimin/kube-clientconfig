# kube-clientconfig

kube-clientconfig reads a [kubeconfig](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/) file and configures an HTTP client for talking with a Kubernetes cluster.

It uses FFI to call the official Go code in https://github.com/kubernetes/client-go/ to configure the authentication headers and TLS settings.
