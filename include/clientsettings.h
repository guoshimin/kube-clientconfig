#include <stdbool.h>
#include <stddef.h>

/* C structs for their golang counterparts */
struct TLSConfig {
  char* CAFile;
  char* CertFile;
  char* KeyFile;

  bool Insecure;
  char* ServerName;

  void* CAData;
  void* CertData;
  void* KeyData;

  size_t CADataLen, CertDataLen, KeyDataLen;
};

struct Header {
  char* name;
  char* value;
};

struct HeaderArray {
  struct Header* headers;
  size_t length;
};

struct ClientSettings {
  struct TLSConfig tls_config;
  struct HeaderArray headers;
  char* error;
};

struct ClientSettings* AllocClientSettings();
struct Header* AllocHeaders(unsigned);
struct Header* HeaderAt(struct Header* base, int n);
