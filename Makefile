CGO_CFLAGS := -I $(CURDIR)/include

generated/lib/libauth.a generated/include/auth.h: go/auth/auth.go go/auth/clientsettings.c include/clientsettings.h
	mkdir -p generated/lib generated/include
	GOPATH=$(CURDIR)/go CGO_CFLAGS="$(CGO_CFLAGS)" $(GO) build -o generated/lib/libauth.a -buildmode=c-archive auth
	mv generated/lib/libauth.h generated/include/auth.h

clean:
	rm -rf generated

.PHONY: go1.4 go1.9

go1.4:
	GOROOT_FINAL=$(GO14ROOT) ./install_go_1_4.sh

go1.9: go1.4
	GOROOT_FINAL=$(GO19ROOT) GOROOT_BOOTSTRAP=$(GO14ROOT) ./install_go_1_9.sh
