#include <stdlib.h>
#include <string.h>
#include "auth.h"
#include "helper.h"

void free_headers(struct Header* hdrs, size_t length) {
  for (size_t i = 0; i < length; i++) {
	free(hdrs[i].name);
	free(hdrs[i].value);
  }
  free(hdrs);
}

void free_client_settings(struct ClientSettings* cs) {
  free_headers(cs->headers.headers, cs->headers.length);
  free(cs->tls_config.CAFile);
  free(cs->tls_config.CertFile);
  free(cs->tls_config.KeyFile);
  free(cs->tls_config.CAData);
  free(cs->tls_config.CertData);
  free(cs->tls_config.KeyData);
  free(cs->tls_config.ServerName);
  free(cs->error);
  free(cs);
}

struct TLSConfig *new_tls_config() {
  struct TLSConfig *ret = (struct TLSConfig*) malloc(sizeof(struct TLSConfig));
  memset(ret, 0, sizeof(struct TLSConfig));
  ret->CAFile = "foobar";
  ret->CAData = (void *) "abc";
  ret->CADataLen = 3;
  return ret;
}

struct TLSConfig *get_tls_config() {
  struct ClientSettings *cs = (struct ClientSettings*) malloc(sizeof(struct ClientSettings));
  memset(cs, 0, sizeof(struct ClientSettings));
  char *err = NULL;
  GetClientSettings(cs, &err);
  return &(cs->tls_config);
}

struct HeaderArray *get_headers() {
  struct ClientSettings *cs = (struct ClientSettings*) malloc(sizeof(struct ClientSettings));
  memset(cs, 0, sizeof(struct ClientSettings));
  char *err = NULL;
  GetClientSettings(cs, &err);
  return &(cs->headers);
}
